基于51uwb新框架实现的TWR定位
具体参考网址:http://51uwb.cn/forum.php?mod=viewthread&tid=165&extra=page%3D1

工程编译说明
1TWR定位共有两种节点，标签和基站
标签是被定位点，基站是固定参考点。

标签和基站的编译是通过宏定义区分的，修改文件bphero_uwb.h

=====================下面实例编译基站0x0002===============
#define RX_NODE
//#define TX_NODE

#ifdef RX_NODE
 #define SHORT_ADDR 0x0002 
 //#define LCD_ENABLE 
#endif  


#ifdef TX_NODE
 #define SHORT_ADDR 0x0013
#endif  
==========================END==============================

基站三个地址固定，分辨是0x0001 0x0002 和0x0003，标签地址无需更改，默认即可

定位过程，需要通过基站0x0001的串口连接电脑，将距离数据送到电脑。

数据格式以及定位操作过程与BP-50一致，具体方法可参见BP-50 用户手册