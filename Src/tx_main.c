/**
  ******************************************************************************
  * @file    Project/STM32F10x_StdPeriph_Template/main.c
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    08-April-2011
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include <stdio.h>

#include "deca_device_api.h"
#include "deca_regs.h"
#include "deca_sleep.h"
#include "port.h"
#include "lcd_oled.h"
#include "frame_header.h"
#include "bp_filter.h"
#include "common_header.h"
#include "bphero_uwb.h"
#include "dwm1000_timestamp.h"
static unsigned char distance_seqnum = 0;
static srd_msg_dsss *msg_f_recv ;
typedef signed long long int64;
typedef unsigned long long uint64;
typedef unsigned int uint32;
/*******************************************************************************************************************/
static uint64 poll_rx_ts;
static uint64 resp_tx_ts;
static uint64 final_rx_ts;

static uint64 poll_tx_ts;
static uint64 resp_rx_ts;
static uint64 final_tx_ts;
static void Send_Dis_To_Anthor0(void);
//MAX_ANTHOR 3 --> 2D
//MAX_ANTHOR 4 --> 3D

#define MAX_ANTHOR 3
//anthor range
#define DEST_BEGIN_ADDR 0x0001
#define DEST_END_ADDR   DEST_BEGIN_ADDR + MAX_ANTHOR - 1 //anthro address 0x001 0x002 0x003 for 2D ,0x0001 0x0002 0x0003 0x0004 for 3D

int Final_Distance[MAX_ANTHOR] = {0};//save all the distance from anthor and tag

char dist_str[16] = {0};
unsigned long time_count = 0;
unsigned long Tag_receive_poll = 0;
uint16 Dest_Address =  DEST_BEGIN_ADDR;

//char delay_factor = 0;
/* Private functions ---------------------------------------------------------*/
void Tx_Simple_Rx_Callback()
{
    uint32 status_reg = 0,i=0;
    uint32 final_tx_time;


    dwt_enableframefilter(DWT_FF_RSVD_EN);//disable recevie
    status_reg = dwt_read32bitreg(SYS_STATUS_ID);
    if (status_reg & SYS_STATUS_RXFCG)
    {
        /* A frame has been received, copy it to our local buffer. */
        frame_len = dwt_read32bitreg(RX_FINFO_ID) & RX_FINFO_RXFL_MASK_1023;
//			    for (i = 0 ; i < FRAME_LEN_MAX; i++ )
//    {
//        rx_buffer[i] = '\0';
//    }
        if (frame_len <= FRAME_LEN_MAX)
        {
            dwt_readrxdata(rx_buffer, frame_len, 0);
            msg_f_recv = (srd_msg_dsss*)rx_buffer;
            msg_f_send.destAddr[0] = msg_f_recv->sourceAddr[0];
            msg_f_send.destAddr[1] = msg_f_recv->sourceAddr[1];

            msg_f_send.seqNum = msg_f_recv->seqNum;

            switch(msg_f_recv->messageData[0])
            {
                case 'A'://Poll ack message
                {
                    //dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_RXFCG | SYS_STATUS_TXFRS);
//					          time_count = portGetTickCnt();
                    resp_rx_ts = get_rx_timestamp_u64();
                    final_tx_time =   dwt_readsystimestamphi32()  + 0x17cdc00/88;//10ms/8
                    //final_tx_time =   dwt_readsystimestamphi32()  + 0x17cdc00/90; fail!!
                    dwt_setdelayedtrxtime(final_tx_time);
                    final_tx_ts = (((uint64)(final_tx_time & 0xFFFFFFFE)) << 8);
                    msg_f_send.messageData[0]='F';//Final message
                    final_msg_set_ts(&msg_f_send.messageData[FINAL_MSG_POLL_TX_TS_IDX], poll_tx_ts);
                    final_msg_set_ts(&msg_f_send.messageData[FINAL_MSG_RESP_RX_TS_IDX], resp_rx_ts);
                    final_msg_set_ts(&msg_f_send.messageData[FINAL_MSG_FINAL_TX_TS_IDX], final_tx_ts);
                    //11 + 1 +4*3 + 1 = 25
//                    dwt_writetxdata(psduLength, (uint8 *)&msg_f_send, 0) ; // write the frame data
//                    dwt_writetxfctrl(psduLength, 0);
									  dwt_writetxdata(25, (uint8 *)&msg_f_send, 0) ; // write the frame data
                    dwt_writetxfctrl(25, 0);
                    dwt_starttx(DWT_START_TX_DELAYED);
                    Final_Distance[(msg_f_send.destAddr[1]<<8)|msg_f_send.destAddr[0] - 1] = (msg_f_recv->messageData[1]*100 + msg_f_recv->messageData[2]);//cm
                    Final_Distance[(msg_f_send.destAddr[1]<<8)|msg_f_send.destAddr[0] - 1] = filter(Final_Distance[(msg_f_send.destAddr[1]<<8)|msg_f_send.destAddr[0] - 1],\
                            (msg_f_send.destAddr[1]<<8)|msg_f_send.destAddr[0] - 1);

//                    printf("Time:%4d 0x%04X <--> 0x%02X%02X :%.02f m\n",portGetTickCnt()-time_count, SHORT_ADDR,msg_f_send.destAddr[1],msg_f_send.destAddr[0],\
//                           ((float)Final_Distance[(msg_f_send.destAddr[1]<<8)|msg_f_send.destAddr[0] - 1]/100));
                    while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS))
                    { };
                }
                break;
                default:
                    break;
            }
        }
    }
    else
    {
        if(!(status_reg & SYS_STATUS_RXRFTO))//timeout以外都认为是数据帧干扰
            Tag_receive_poll = 1;//数据帧干扰
        dwt_write32bitreg(SYS_STATUS_ID, (SYS_STATUS_RXFCG | SYS_STATUS_ALL_RX_ERR|SYS_STATUS_TXFRS));
    }
    if( Dest_Address == DEST_BEGIN_ADDR)
    {
        Send_Dis_To_Anthor0();
    }
}


void BPhero_Distance_Measure_Specail_TAG(void)
{
    Dest_Address++;
    if(Dest_Address == DEST_END_ADDR+1)
    {
        Dest_Address = DEST_BEGIN_ADDR;
    }
	
    msg_f_send.destAddr[0] =(Dest_Address) &0xFF;
    msg_f_send.destAddr[1] =  ((Dest_Address)>>8) &0xFF;

    msg_f_send.seqNum = distance_seqnum;
    msg_f_send.messageData[0]='P';//Poll message
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, !GPIO_PIN_RESET);//PA node ,enable pa
		
		dwt_writetxdata(12,(uint8 *)&msg_f_send, 0) ;  // write the frame data
    dwt_writetxfctrl(12, 0);

//    dwt_writetxdata(TAG_POLL_MSG_LEN,(uint8 *)&msg_f_send, 0) ;  // write the frame data
//    dwt_writetxfctrl(TAG_POLL_MSG_LEN, 0);
    dwt_starttx(DWT_START_TX_IMMEDIATE);

    while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS))
    { };
		dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_RXFCG | SYS_STATUS_TXFRS);
    poll_tx_ts = get_tx_timestamp_u64();
		
		for (int i = 0 ; i < FRAME_LEN_MAX; i++ )
    {
        rx_buffer[i] = '\0';
    }
    dwt_enableframefilter(DWT_FF_DATA_EN);
    dwt_setrxtimeout(RESP_RX_TIMEOUT_UUS*10);
    dwt_rxenable(0);

    if(++distance_seqnum == 255)
        distance_seqnum = 0;

}
/************************************************************************/
/************************************************************************/
/************************使用定时器周期性启动测距************************/
/********************************BEGIN***********************************/
/************************************************************************/
TIM_HandleTypeDef  htim3;
#define MAX_FREQ_HZ 95 //定位2HZ，需要与3个节点测试2次，共计需要6次,间隔大约166ms
#define MAX_TX_Node 20
#define  TIM3_ReLoad   (int)(10000/(MAX_FREQ_HZ*MAX_ANTHOR))//reload 间隔，1发送节点2hz reload about 166ms
//#define  TIM3_ReLoad  38
#define  TIM3_Delay_Step   (int)((TIM3_ReLoad)/MAX_TX_Node)//多个tx节点，每个节点之间的delay

static void MX_TIM3_Init(uint32 reload)
{
    TIM_ClockConfigTypeDef sClockSourceConfig;
    TIM_MasterConfigTypeDef sMasterConfig;

    htim3.Instance = TIM3;
    htim3.Init.Prescaler = 8400-1;//10HZ freq
    htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim3.Init.Period = reload;
    htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
    {
        _Error_Handler(__FILE__, __LINE__);
    }

    sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
    if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
    {
        _Error_Handler(__FILE__, __LINE__);
    }

    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
    {
        _Error_Handler(__FILE__, __LINE__);
    }
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @param  htim: TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if (htim->Instance == htim3.Instance)
    {
        HAL_TIM_Base_Stop(&htim3);
        /* Toggle LED */
        if( Tag_receive_poll ==  0)
        {
            dwt_forcetrxoff();
            //clear last time all flag
           // dwt_write32bitreg(SYS_STATUS_ID, (SYS_STATUS_RXFCG | SYS_STATUS_ALL_RX_ERR|SYS_STATUS_TXFRS));
            BPhero_Distance_Measure_Specail_TAG();
					  if(Dest_Address == DEST_BEGIN_ADDR)
            {
                HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4);
            }
            time_count = portGetTickCnt();

            HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_8);
            TIM3->ARR = TIM3_ReLoad;
        }
        else
        {
            HAL_TIM_Base_Stop(&htim3);
            TIM3->ARR = TIM3_Delay_Step*((SHORT_ADDR%10)+1);//random delay
            Tag_receive_poll = 0;
        }
        HAL_TIM_Base_Start(&htim3);
    }
}
/************************使用定时器周期性启动测距************************/
/*********************************END***********************************/


/************************************************************************/
/************************************************************************/
/************************使用定LCD 显示调试距离信息***********************/
/*********************调试完毕关闭LCD显示提高刷新频率*********************/
/********************************BEGIN***********************************/
/************************************************************************/

static void LCD_Display_Distance(void)
{
    char dist_str[16] = {0};
    if(Final_Distance[0]>0)
    {
        sprintf(dist_str, "an1:%3.2fm      ", (float)Final_Distance[0]/100);
        OLED_ShowString(0, 2,dist_str);
    }
    if(Final_Distance[1]>0)
    {
        sprintf(dist_str, "an2:%3.2fm      ", (float)Final_Distance[1]/100);
        OLED_ShowString(0, 4,dist_str);
    }
    if(Final_Distance[2]>0)
    {
        sprintf(dist_str, "an3:%3.2fm      ", (float)Final_Distance[2]/100);
        OLED_ShowString(0, 6,dist_str);
    }
}
/**********************************END***********************************/
/************************************************************************/

//**************************************************************//
//distance1 anthor0 <--> TAG  cm
//distance2 anthor1 <--> TAG  cm
//distance3 anthor2 <--> TAG  cm
//将三个距离信息发送给基站1--> 0x0001
//如果用其它无限模块通信，可以将这三个距离信息通过其它无线模块传输给电脑
//**************************************************************//
static void Send_Dis_To_Anthor0(void)
{
    static int framenum = 0 ;
    //only send this message to anthor0:short address equal 0x0001
    msg_f_send.destAddr[0] =(0x0001) &0xFF;
    msg_f_send.destAddr[1] =  ((0x0001)>>8) &0xFF;

    msg_f_send.seqNum = distance_seqnum;
    msg_f_send.messageData[0]='M';//Poll message
    {
        uint8 len = 0;
        uint8 LOCATION_INFO_START_IDX = 1;
        msg_f_send.messageData[LOCATION_INFO_START_IDX + (len++)] = 'm';
        msg_f_send.messageData[LOCATION_INFO_START_IDX + (len++)] = 'r';
        msg_f_send.messageData[LOCATION_INFO_START_IDX + (len++)] = 0x02;
        msg_f_send.messageData[LOCATION_INFO_START_IDX + (len++)] = SHORT_ADDR;//TAG ID
        msg_f_send.messageData[LOCATION_INFO_START_IDX + (len++)] = (uint8)(framenum&0xFF);
        msg_f_send.messageData[LOCATION_INFO_START_IDX + (len++)] = (uint8)((framenum>>8)&0xFF);

        msg_f_send.messageData[LOCATION_INFO_START_IDX + (len++)] = (uint8)((Final_Distance[0]>0?Final_Distance[0]:0xFFFF)&0xFF);
        msg_f_send.messageData[LOCATION_INFO_START_IDX + (len++)] = (uint8)(((Final_Distance[0]>0?Final_Distance[0]:0xFFFF) >>8)&0xFF);

        msg_f_send.messageData[LOCATION_INFO_START_IDX + (len++)] = (uint8)((Final_Distance[1]>0?Final_Distance[1]:0xFFFF)&0xFF);
        msg_f_send.messageData[LOCATION_INFO_START_IDX + (len++)] = (uint8)(((Final_Distance[1]>0?Final_Distance[1]:0xFFFF) >>8)&0xFF);

        msg_f_send.messageData[LOCATION_INFO_START_IDX + (len++)] = (uint8)((Final_Distance[2]>0?Final_Distance[2]:0xFFFF)&0xFF);
        msg_f_send.messageData[LOCATION_INFO_START_IDX + (len++)] = (uint8)(((Final_Distance[2]>0?Final_Distance[2]:0xFFFF) >>8)&0xFF);

        if(MAX_ANTHOR > 3)
        {
            msg_f_send.messageData[LOCATION_INFO_START_IDX + (len++)] = (uint8)((Final_Distance[MAX_ANTHOR-1]>0?Final_Distance[MAX_ANTHOR-1]:0xFFFF)&0xFF);
            msg_f_send.messageData[LOCATION_INFO_START_IDX + (len++)] = (uint8)(((Final_Distance[MAX_ANTHOR-1]>0?Final_Distance[MAX_ANTHOR-1]:0xFFFF) >>8)&0xFF);
        }
        else
        {
            msg_f_send.messageData[LOCATION_INFO_START_IDX + (len++)] = (uint8)((Final_Distance[0]>0?Final_Distance[0]:0xFFFF)&0xFF);
            msg_f_send.messageData[LOCATION_INFO_START_IDX + (len++)] = (uint8)(((Final_Distance[0]>0?Final_Distance[0]:0xFFFF) >>8)&0xFF);
        }

        msg_f_send.messageData[LOCATION_INFO_START_IDX + (len++)] = '\n';
        msg_f_send.messageData[LOCATION_INFO_START_IDX + (len++)] = '\r';
    }
	
//    dwt_forcetrxoff();
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, !GPIO_PIN_RESET);//PA node ,enable pa
    //take care of data len!!
//    dwt_writetxdata(TAG_POLL_MSG_LEN + 16,(uint8 *)&msg_f_send, 0) ;  // write the frame data
//    dwt_writetxfctrl(TAG_POLL_MSG_LEN + 16, 0);
		dwt_writetxdata(11 + 16,(uint8 *)&msg_f_send, 0) ;  // write the frame data
    dwt_writetxfctrl(11 + 16, 0);

    dwt_starttx(DWT_START_TX_IMMEDIATE);

    while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS))
    { };
	
	//printf("Time:%4d\n",portGetTickCnt()-time_count);
    framenum++;

    //LCD_Display_Distance();
}


int tx_main(void)
{
    OLED_ShowString(0,0,"   51UWB Node");
    sprintf(dist_str, "    Tx Node");
    OLED_ShowString(0,2,dist_str);
    OLED_ShowString(0,6,"  www.51uwb.cn");

    bphero_setcallbacks(Tx_Simple_Rx_Callback);
    /* Infinite loop */
    printf("Tx Node,TIM3_ReLoad = %d\n",TIM3_ReLoad);

    MX_TIM3_Init(TIM3_ReLoad);
    HAL_TIM_Base_Start_IT(&htim3);
    while(1)
    {
    }
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif

/**
  * @}
  */
/*****************************************************************************************************************************************************
 * NOTES:
 *
 * 1. In this example, maximum frame length is set to 127 bytes which is 802.15.4 UWB standard maximum frame length. DW1000 supports an extended
 *    frame length (up to 1023 bytes long) mode which is not used in this example.
 * 2. Manual reception activation is performed here but DW1000 offers several features that can be used to handle more complex scenarios or to
 *    optimise system's overall performance (e.g. timeout after a given time, automatic re-enabling of reception in case of errors, etc.).
 * 3. We use polled mode of operation here to keep the example as simple as possible but RXFCG and error/timeout status events can be used to generate
 *    interrupts. Please refer to DW1000 User Manual for more details on "interrupts".
 * 4. The user is referred to DecaRanging ARM application (distributed with EVK1000 product) for additional practical example of usage, and to the
 *    DW1000 API Guide for more details on the DW1000 driver functions.
 ****************************************************************************************************************************************************/
/**
  * @}
  */
/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
